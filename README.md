# cpp-template-sonarqube

template for sonarqube

## build command

`msbuild .\hello.sln > output.log`

## drmemory command

`drmemory -logdir drmemory -- .\build\Release-x64\hello.exe "10" "10" "200" "0" "0"`

## cppcheck command

`cppcheck.exe -v -f --language=c++ --platform=win64 --enable=all --xml --xml-version=2 -I. -idependencies . 2> cppcheck.xml`

## sonar-scanner command

`sonar-scanner`
